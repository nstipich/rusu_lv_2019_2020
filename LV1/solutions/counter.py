numbers = []

while(True):
    x = input("Unesite vrijednost: ")
    if x == "Done":
        break
    try:
        numbers.append(float(x))
    except ValueError:
        print("Niste unijeli broj!")

print("\nUneseno brojeva je ", len(numbers))
print("\nMinimalna unesena vrijednost je ", min(numbers))
print("\nMaksimalna unesena vrijednost je ", max(numbers))
print("\nSrednja vrijednost unesenog niza je ", sum(numbers)/len(numbers))