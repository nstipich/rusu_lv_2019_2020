import sys

grade = input("Unesite bodove: ")

try:
    grade = float(grade)
except ValueError:
    print("Unesite broj!")
    sys.exit()
grade_output = ""
if(grade >= 0.0 and grade <=1.0):
    if grade < 0.6:
                grade_output = "F"
    elif grade >= 0.6 and grade < 0.7:
                grade_output = "D"
    elif grade >= 0.7 and grade < 0.8:
                grade_output = "C"
    elif grade >= 0.8 and grade < 0.9:
                grade_output = "B"
    else:
                grade_output = "A"
else:
        print("Koeficijent bodova je izvan [0.0-1.0]")
        sys.exit()
print("Vasa ocjena je: ", grade_output)        