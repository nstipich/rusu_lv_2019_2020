from matplotlib import pyplot as plt
import numpy as np
import sklearn.linear_model as lin_mod
import sklearn.metrics as met
from sklearn.preprocessing import PolynomialFeatures

def plot_confusion_matrix(c_matrix):
    norm_conf = []
    for i in c_matrix:
        a = 0
        tmp_arr = []
        a = sum(i, 0)
        for j in i:
            tmp_arr.append(float(j) / float(a))
        norm_conf.append(tmp_arr)

    fig = plt.figure()
    ax = fig.add_subplot(111)
    res = ax.imshow(np.array(norm_conf), cmap=plt.cm.Greys, interpolation='nearest')

    width = len(c_matrix)
    height = len(c_matrix[0])

    for x in range(width):
        for y in range(height):
            ax.annotate(str(c_matrix[x][y]), xy=(y, x),
                        horizontalalignment='center',
                        verticalalignment='center', color='green', size=20)

    fig.colorbar(res)
    numbers = '0123456789'
    plt.xticks(range(width), numbers[:width])
    plt.yticks(range(height), numbers[:height])

    plt.ylabel('Stvarna klasa')
    plt.title('Predvideno modelom')
    plt.show()


def generate_data(n):
    # prva klasa
    n1 = int(n / 2)
    x1_1 = np.random.normal(0.0, 2, (n1, 1));
    # x1_1 = .21*(6.*np.random.standard_normal((n1,1)));
    x2_1 = np.power(x1_1, 2) + np.random.standard_normal((n1, 1));
    y_1 = np.zeros([n1, 1])
    temp1 = np.concatenate((x1_1, x2_1, y_1), axis=1)

    # druga klasa
    n2 = int(n - n / 2)
    x_2 = np.random.multivariate_normal((0, 10), [[0.8, 0], [0, 1.2]], n2);
    y_2 = np.ones([n2, 1])
    temp2 = np.concatenate((x_2, y_2), axis=1)

    data = np.concatenate((temp1, temp2), axis=0)

    # permutiraj podatke
    indices = np.random.permutation(n)
    data = data[indices, :]

    return data


poly = PolynomialFeatures(3, include_bias=False)

np.random.seed(242)
train_data = generate_data(200)

train_data_new = poly.fit_transform(train_data[:, 0:2])

x1_train_data = train_data_new[:, 0]
x2_train_data = train_data_new[:, 1]
y_train_data = train_data[:, 2]
x_train_data = train_data_new[:, 0:2]

np.random.seed(12)
test_data = generate_data(100)

train_data_new = poly.fit_transform(test_data[:, 0:2])

x1_test_data = train_data_new[:, 0]
x2_test_data = train_data_new[:, 1]
y_test_data = test_data[:, 2]
x_test_data = train_data_new[:, 0:2]

model = lin_mod.LogisticRegression()
model.fit(x_train_data, y_train_data)
model.predict(x_train_data)

parameter1 = model.intercept_
parameter2 = model.coef_

y_test_predict_data = model.predict(x_test_data)


# 2. task
plt.figure(1)
plt.scatter(x1_train_data, x2_train_data, c=y_train_data)
plt.show()
# *****

# 3. task
plt.figure(2)
plt.scatter(x1_train_data, x2_train_data, c=y_train_data)
x2 = (-parameter1 - parameter2[0][0] * x1_train_data) / parameter2[0][1]
plt.plot(x1_train_data, x2)
plt.show()
# *****


# 4. task
plt.figure(3)
f, ax = plt.subplots(figsize=(8, 6))
x_grid, y_grid = np.mgrid[min(train_data[:, 0])-0.5:max(train_data[:, 0])+0.5:.05,
                          min(train_data[:, 1])-0.5:max(train_data[:, 1])+0.5:.05]
grid = np.c_[x_grid.ravel(), y_grid.ravel()]
probs = model.predict_proba(grid)[:, 1].reshape(x_grid.shape)

cont = ax.contourf(x_grid, y_grid, probs, 60, cmap="Greys", vmin=0, vmax=1)

ax_c = f.colorbar(cont)
ax_c.set_label("$P(y = 1|\mathbf{x})$")
ax_c.set_ticks([0, .25, .5, .75, 1])
ax.set_xlabel('$x_1$', alpha=0.9)
ax.set_ylabel('$x_2$', alpha=0.9)
ax.set_title('Izlaz logisticke regresije')
plt.show()
# *****


# 5. task
plt.figure(4)
colors = []
for i, value in enumerate(y_test_predict_data):
    if value != y_test_data[i]:
        colors.append('green')
    else:
        colors.append('black')

plt.scatter(x1_test_data, x2_test_data, c=colors)
plt.show()
# *****

# 6. task
confusion = met.confusion_matrix(y_test_data, y_test_predict_data)
plot_confusion_matrix(confusion)

tp = confusion[0][0]
fp = confusion[0][1]
fn = confusion[1][0]
tn = confusion[1][1]

acc = met.accuracy_score(y_test_data, y_test_predict_data)
prec = met.precision_score(y_test_data, y_test_predict_data)
rec = met.recall_score(y_test_data, y_test_predict_data)
miss_rate = 1 - acc
spec = tn / (tn + fp)
# *****

